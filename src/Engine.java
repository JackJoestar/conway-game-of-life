import java.awt.Graphics;
import java.util.Random;

public class Engine {
	public static Cell[][] cell;
	public static Cell[][] cell2;
	
	public Engine() {
		cell = new Cell[Component.width/Cell.cellSize][Component.height/Cell.cellSize];
		cell2 = new Cell[Component.width/Cell.cellSize][Component.height/Cell.cellSize];
		for(int x = 0; x < cell.length; x++) {
			for(int y = 0; y < cell[x].length; y++) {
				
				//Drops a random initial seed.
				cell[x][y] = new Cell(x,y);
				cell2[x][y] = new Cell(x,y);
				cell[x][y].isAlive = new Random().nextInt(4) == 0;

				
				
			}
			
		}
	}
	
	public void tick() {
		//Copy the second array with the first
		for(int x = 0; x < cell.length; x++) {
			for(int y = 0; y < cell[0].length; y++) {
				cell2[x][y].isAlive = cell[x][y].isAlive;
				
			}
		}
	
		//"Delete" all the cells in the first array
		for(int x = 0; x < cell.length; x++) {
			for(int y = 0; y < cell[0].length; y++) {
				cell[x][y].isAlive = false;
			}
		}
		
		//Employ the rules and apply them to the first array
		for(int x = 0; x < cell.length; x++) {
			for(int y = 0; y < cell[0].length; y++) {
				
				//Check its surroundings
				int n = 0;
				try {
					
					if(cell2[x+1][y].isAlive) n++;
					else if(cell2[x][y+1].isAlive) n++;
					else if(cell2[x+1][y+1].isAlive) n++;
					else if(cell2[x-1][y].isAlive) n++;
					else if(cell2[x][y-1].isAlive) n++;
					else if(cell2[x-1][y-1].isAlive) n++;
					else if(cell2[x+1][y-1].isAlive) n++;
					else if(cell2[x-1][y+1].isAlive) n++;
		
				}catch(Exception e) {}//Avoids the IndexOutOfBoundsException


				boolean alive = false;
				
					
				//Employ the rules
				if(cell2[x][y].isAlive) {
					
					switch(n) {
					case 0:
					case 1:
						alive = false;
						break;
					case 2:
					case 3:
						alive = true;
						break;
					default:
						alive = false;
						break;
					    } 
					
					}
				else {
						switch(n) {
						case 3:
							alive = true;
							break;
						default:
							alive = false;
							break;
						}
				}
				cell[x][y].isAlive = alive;
				
				
			}
		}
	}
	
	
	public void render(Graphics g) {
		for(int x = 0; x < cell.length; x++) {
			for(int y = 0; y < cell[0].length; y++) {
				cell[x][y].render(g);
			}
		}
	}
}
