/*Conway's Game of Life
 * 
 *Game of Life is a zero player game all about cellular automation.
 *
 */

import java.applet.Applet;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JFrame;

public class Component extends Applet implements Runnable {

	private static final long serialVersionUID = 1L;
	public static int width = 940;
	public static int height = 940;
	public static final String title = "Conway\'s Game of Life";
	public static boolean isRunning = false;
	public static Image screen;
	public static Graphics g2;
	public static Engine grid;
	
	public void start() {
		init();
		isRunning = true;
		Thread thread = new Thread();
		thread.start();
		run();
	}
	
	public void init() {
		screen = createVolatileImage(width,height);
		screen.flush();
		grid = new Engine();
	}
	

	
	public void run() {
		while(isRunning) {
			tick();
			render(g2);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {}
		}
		
	}
	
	public void tick() {
		width = getWidth();
		height = getHeight();
		grid.tick();
	}
	
	public void render(Graphics g) {
		screen = createVolatileImage(width, height);
		g = screen.getGraphics();
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, width, height);
		
		grid.render(g);
		
		g = getGraphics();
		g.drawImage(screen, 0, 0, width, height, null);
		
	}
	
	public Component() {
		
	}


	
	public static JFrame frame;
	public static void main(String[] args) {
		Component component = new Component();
		frame = new JFrame();
		frame.add(component);
		frame.setTitle(title);
		frame.setSize(width, height);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		component.start();
	}
}
